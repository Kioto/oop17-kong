# Project Title
Repository for "Donky Kong", a Java reissue of the famous Donkey Kong platform developed by Nintendo in 1981.
The project has been corrected and discussed with the professor, the score given is 28/30

## Built With

* Java Swing - Graphical User Interface
* JUnit - Testing

## Authors

* **Rispoli Luca** - *Characters' model and view features* 
* **Budini Elizabeta** - *Scores and main menu view* 
* **Samuele Gregori** - *Gameloop and barrels features* 
* **Marco Creta** - *Model and Environment features* 

## Contacts

* luca.rispoli@studio.unibo.it
* elizabeta.budini@studio.unibo.it
* samuele.gregori@studio.unibo.it
* marco.creta@studio.unibo.it

## Instructions

Download and run DonkyKong.jar . 
Please make sure you have a working JRE installed.


